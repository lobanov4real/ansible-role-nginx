[![Project Status: Active – The project has reached a stable, usable state and is being actively developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![Ansible Lint](https://img.shields.io/badge/Ansible-Lint-purple.svg)](https://ansible.readthedocs.io/projects/lint/)
[![Molecule test](https://img.shields.io/badge/Molecule-Test-purple.svg)](https://ansible.readthedocs.io/projects/molecule/)
[![GitLab CI](https://gitlab.com/lobanov4real/ansible-role-nginx/badges/main/pipeline.svg)](https://gitlab.com/lobanov4real/ansible-role-nginx/-/pipelines)
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/license/mit/)

# Ansible NGINX Role

This role installs NGINX Open Source on your target host.

**Note:** This role is still in active development. There may be unidentified issues and the role variables may change as development continues.

## Requirements

- This role is developed and tested with [maintained](https://docs.ansible.com/ansible/latest/reference_appendices/release_and_maintenance.html) versions of `ansible-core` **2.16**.
- When using `ansible-core`, you will also need to install the following collections:

```yaml
---
collections:
  - name: ansible.posix
    version: 1.5.4
  - name: community.general
    version: 6.4.0
  - name: community.docker # Only required if you plan to use Molecule
    version: 3.4.7
```

- You will need to run this role as a root user using Ansible's `become` parameter. Make sure you have set up the appropriate permissions on your target hosts.
- Instructions on how to install Ansible can be found in the [Ansible website](https://docs.ansible.com/ansible/latest/installation_guide/index.html).

### Jinja2

- This role uses Jinja2 templates. `ansible-core` installs Jinja2 by default, but depending on your install and/or upgrade path, you might be running an outdated version of Jinja2. The minimum version of Jinja2 required for the role to properly function is **3.1**.
- Instructions on how to install Jinja2 can be found in the [Jinja2 website](https://jinja.palletsprojects.com/en/3.1.x/intro/#installation).

### Molecule (optional)

- Molecule is used to test the various functionalities of the role. The recommended version of Molecule to test this role is **6.x.x**.
- Instructions on how to install Molecule can be found in the [Molecule website](https://ansible.readthedocs.io/projects/molecule/installation/). You will also need to install the Molecule Docker driver.

## Installation

### Git

To pull the latest edge commit of the role from GitLab, use:

```shell
git clone https://gitlab.com/lobanov4real/ansible-role-nginx.git
```

## Platforms

The NGINX Ansible role was test in next platforms:

```yaml
Debian:
  - bullseye (11)
Oracle Linux:
  - 9
Red Hat:
  - 9
SUSE:
  - 15.5
Amazon Linux:
  - 2
AlmaLinux:
  - 8
Ubuntu:
  - jammy (22.04)
```

## Role variables

The preset variables can be found in the vars/main.yml:

```yaml
---
## OS dependencies

# Alpine dependencies
nginx_alpine_dependencies: [ca-certificates, coreutils, openssl, pcre2]

# Debian dependencies
nginx_debian_dependencies: [apt-transport-https, ca-certificates, gpg-agent]

# Red Hat dependencies
nginx_redhat_dependencies: [ca-certificates]

# SLES dependencies
nginx_sles_dependencies: [ca-certificates]
```

## Example Playbook

Working functional playbook example can be found in the molecule/converge.yml:

```yaml
---
- name: Converge
  hosts: all
  tasks:
    - name: NGINX installation for Linux.
      ansible.builtin.include_role:
        name: lobanov4real.nginx
```

## License

MIT

## Additional Information

I use [NGINX Official Ansible Role](https://github.com/nginxinc/ansible-role-nginx) like general **manual** to build mine.
