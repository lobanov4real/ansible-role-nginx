# Changelog

## v1.9 (January 8th, 2024)

- Refactoring.
- Fill README.md.
- Delete SELinux.
- Add Ubuntu distro to Molecule test.

## v1.8 (December 24th, 2023)

- Add RHEL and Amazon Linux distro support.
- Add info about tested distro to meta.

## v1.7 (December 22th, 2023)

- Add openSUSE leap support
- Add Changelog
